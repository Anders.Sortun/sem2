# Mitt program

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.

Start programmet med å kjøre Main.java (sem2\src\main\java\no\uib\inf101\sem2\Main.java), du vil bli møtt med en hvit skjerm med en grønn rektangel. Spillet er nå i gang.

Du som spiller kontrollerer den grønne rektangelen, med piltastene for opp, ned, ventre og høyre, og spacebar for å skyte.

Fiender vil komme fra høyre side og ditt mål er å skyte de, før de skyter deg. Fiender har 5 healthpoints, representert med hvilke farge de er. Fra blå til rød, hvor blå er 5 healthpoints og rød er 1.

Du som spiller starter med 3 liv, og vil ha 3 sekunder med "skjold" hver gang du blir truffet. Dette gjelder ikke hvis du kræsjer med en fiende, du vil ta liv uansett, men fienden vil miste alle livene.

Hver fiende du dreper vil gi deg 50 poeng. Nye fiender kommer hvert 6 sekund. 
Når scoren din blir mer enn 500, vil to fiender komme hvert 6 sekund, og ved 1000 vil 3 fiender komme.
Hvor mange fiender som kommer hvert 6 sekund øker med 1 sekund for hver 500 is score.

Du blir møtt med en Game Over skjerm hvis livene dine kommer til 0, og en knapp for å begynne på nytt.

Video til gameplay: https://youtu.be/bISgyQ1Lv3Q


