package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.controller.PlayerController;
import no.uib.inf101.sem2.model.Elements.Enemy;
import no.uib.inf101.sem2.model.Elements.StraightProjectile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Dimension;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;


public class GameModelTest {
    
    @Test
    void GameModelSanityTest(){
        Dimension dim = new Dimension(100, 100);
        GameModel GM = new GameModel(dim, new EventBus());
        assertEquals(new Dimension(100, 100), GM.getPlayableArea());
        assertEquals(GameState.ACTIVE, GM.getGameState());
        assertEquals(0, GM.getScore());
        assertEquals(new ArrayList<>(),GM.getEnemies());
        assertEquals(new ArrayList<>(),GM.getProjectiles());
        PlayerController PC = GM.getPlayerController();
        assertFalse(PC.getMoveDown());    
        assertFalse(PC.getMoveUp());    
        assertFalse(PC.getMoveLeft());    
        assertFalse(PC.getMoveRight());    
    }

    @Test
    void GameModelRestartTest(){
        Dimension dim = new Dimension(100, 100);
        GameModel GM = new GameModel(dim, new EventBus());
        GM.getEnemies().add(new Enemy(null, 0, dim, dim, new Coordinate(0, 0)));
        GM.getProjectiles().add(new StraightProjectile(dim, dim, new Coordinate(0, 0), 0, null));
        GM.restart();
        assertEquals(new ArrayList<>(), GM.getEnemies());
        assertEquals(new ArrayList<>(), GM.getProjectiles());
    }

    @Test
    void IntersectionTest(){
        Dimension dim = new Dimension(100, 100);
        GameModel GM = new GameModel(dim, new EventBus());
        GM.getProjectiles().add(new StraightProjectile(dim, new Dimension(10, 10), GM.getPlayer().getCoordinate(), 0, "Enemy"));
        GM.clockTick();
        assertEquals(new ArrayList<>(), GM.getProjectiles());
        assertEquals(2,GM.getPlayer().getHealthPoints());
        GM.getProjectiles().add(new StraightProjectile(dim, new Dimension(10, 10), GM.getPlayer().getCoordinate(), 0, "Player"));
        assertEquals(1,GM.getProjectiles().size());
        assertEquals(2,GM.getPlayer().getHealthPoints());
    }

    @Test
    void PlayerMoveTest(){
        Dimension dim = new Dimension(100, 100);
        GameModel GM = new GameModel(dim, new EventBus());
        Coordinate playerCoor = GM.getPlayer().getCoordinate();
        GM.movePlayer(5, 5);
        assertEquals(new Coordinate(playerCoor.y()+5,playerCoor.x()+5), GM.getPlayer().getCoordinate());
    }

    @Test
    void RemoveDeadEnemiesTest(){
        Dimension dim = new Dimension(100, 100);
        EventBus eb = new EventBus();
        GameModel GM = new GameModel(dim, eb);
        GM.getEnemies().add(new Enemy(eb, 1, dim, new Dimension(10, 10), new Coordinate(0, 0)));
        GM.getProjectiles().add(new StraightProjectile(dim, new Dimension(1, 1), new Coordinate(0, 0), 0, "Player"));
        GM.clockTick();
        assertEquals(0, GM.getEnemies().size());
    }

    @Test
    void CheckIfGameOverTest(){
        Dimension dim = new Dimension(100, 100);
        GameModel GM = new GameModel(dim, new EventBus());
        GM.getPlayer().takesDamage();
        GM.getPlayer().takesDamage();
        GM.getPlayer().takesDamage();
        GM.clockTick();
        assertEquals(GameState.GAME_OVER, GM.getGameState());
    }
}
