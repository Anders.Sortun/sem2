package no.uib.inf101.sem2.model.Elements;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Dimension;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.Coordinate;
import no.uib.inf101.sem2.model.EventBus;


public class EnemyTest {
    
    @Test
    void EnemySanityTest(){
        EventBus eb = new EventBus();
        int healthPoints = 3;
        Dimension moveableArea = new Dimension(10, 10);
        Dimension hitBoxDimensions = new Dimension(2, 2);
        Coordinate coordinate = new Coordinate(0,0);
        Enemy newEnemy = new Enemy(eb, healthPoints, moveableArea, hitBoxDimensions, coordinate);
        assertEquals(healthPoints, newEnemy.getHealthPoints());
        assertEquals(moveableArea, newEnemy.getMoveableArea());
        assertEquals(hitBoxDimensions, newEnemy.getHitBoxDimensions());
        assertEquals(coordinate, newEnemy.getCoordinate());
    }

    @Test
    void EnemyTakesDamage(){
        EventBus eb = new EventBus();
        int healthPoints = 5;
        Dimension moveableArea = new Dimension(10, 10);
        Dimension hitBoxDimensions = new Dimension(2, 2);
        Coordinate coordinate = new Coordinate(0,0);
        Enemy newEnemy = new Enemy(eb, healthPoints, moveableArea, hitBoxDimensions, coordinate);
        newEnemy.takesDamage(-1);
        assertEquals(5,newEnemy.getHealthPoints());
        newEnemy.takesDamage(1);
        assertEquals(4,newEnemy.getHealthPoints());
        newEnemy.takesDamage(5);
        assertEquals(0,newEnemy.getHealthPoints());
    }
}
