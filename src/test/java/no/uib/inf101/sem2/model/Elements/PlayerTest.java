package no.uib.inf101.sem2.model.Elements;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.Coordinate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

public class PlayerTest {
    

    @Test
    void PlayerSanityTest(){
        Dimension dim = new Dimension(0,0);
        Coordinate coor = new Coordinate(0, 0);
        Player newPlayer = new Player(0, dim, dim, coor);
        assertEquals(0, newPlayer.getHealthPoints());
        assertEquals(dim, newPlayer.getHitBoxDimensions());
        assertEquals(coor, newPlayer.getCoordinate());
    }

    @Test
    void TestMovePlayer(){
        Dimension moveAbleArea = new Dimension(10,10);
        Dimension hitBoxDimensions = new Dimension(2,2);
        Coordinate coor = new Coordinate(0, 0);
        Player newPlayer = new Player(0, moveAbleArea, hitBoxDimensions, coor);
        assertTrue(newPlayer.shiftCoordinateBy(5, 5));
        assertEquals(new Coordinate(5,5), newPlayer.getCoordinate());

        //Moves hitBox out of bounds, should return false, and not change players coordinates
        assertFalse(newPlayer.shiftCoordinateBy(5, 5));
        assertEquals(new Coordinate(5,5), newPlayer.getCoordinate());

    }

    @Test
    void TestPlayerTakesDamage(){
        Dimension moveAbleArea = new Dimension(10,10);
        Dimension hitBoxDimensions = new Dimension(2,2);
        Coordinate coor = new Coordinate(0, 0);
        Player newPlayer = new Player(3, moveAbleArea, hitBoxDimensions, coor);
        newPlayer.takesDamage();
        assertEquals(2, newPlayer.getHealthPoints());
        newPlayer.takesDamage();
        assertEquals(1, newPlayer.getHealthPoints());
        newPlayer.takesDamage();
        assertEquals(0, newPlayer.getHealthPoints());
        //Should not continue to take damage after reaching zero.
        newPlayer.takesDamage();
        assertEquals(0, newPlayer.getHealthPoints());
    }

    @Test
    void TestIfHitBoxMoves(){
        Dimension moveAbleArea = new Dimension(10,10);
        Dimension hitBoxDimensions = new Dimension(2,2);
        Coordinate coor = new Coordinate(0, 0);
        Player newPlayer = new Player(0, moveAbleArea, hitBoxDimensions, coor);
        assertEquals(new Rectangle2D.Double(0, 0, 2, 2), newPlayer.getHitBox());
        
        newPlayer.shiftCoordinateBy(2, 2);
        assertEquals(new Rectangle2D.Double(2, 2, 2, 2), newPlayer.getHitBox());
    }
}
