package no.uib.inf101.sem2.view;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Dimension;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Elements.Enemy;
import no.uib.inf101.sem2.model.Elements.Player;
import no.uib.inf101.sem2.model.Elements.StraightProjectile;

/**
 * Object which collects values of interest from the model, and displays them on the screen.
 */
public class GameView extends JPanel{
    
    ViewableGameModel model;
    ColorInterpreter CI;

    /**
     * The constructor for GameView, takes a ViewableGameModel as a parameter.
     * @param model A ViewableGameModel, representing the game on a logical level.
     */
    public GameView(ViewableGameModel model){
        this.setFocusable(true);
        this.model = model;
        this.CI = new ColorInterpreter();
        this.setPreferredSize(new Dimension((int)model.getPlayableArea().getWidth(),(int)model.getPlayableArea().getHeight()+20));
    }

    /**
     * Draws game by calling to all helper methods.
     */
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        drawPlayer(g2);
        drawEnemies(g2);
        drawProjectiles(g2);
        drawHealthAndScore(g2);
        if(model.getGameState()==GameState.GAME_OVER){
            drawGameOverScreen(g2);
        }
    }

    private JButton restartButton = new JButton("Restart");
    private void drawGameOverScreen(Graphics2D g) {
        g.setColor(CI.getGameOverScreenColor());
        Rectangle2D rect = getBounds();
        g.fill(rect);
        g.setColor(Color.BLACK);
        g.draw(rect);
        g.setFont(CI.getDefaultFont());
        Inf101Graphics.drawCenteredString(g, "GAME OVER", rect);
        restartButton.addActionListener(this::restartGame);
        restartButton.setBounds((int)this.getBounds().getWidth()/2-50, (int)this.getBounds().getHeight()/2 + 25, 100, 50);
        this.add(restartButton);
    }

    private void restartGame(ActionEvent l){
        this.remove(restartButton);
        this.model.restart();

    }

    private void drawPlayer(Graphics2D g){
        g.setColor(CI.getPlayerColor());
        Player p = model.getPlayer();
        g.fill(p.getHitBox());
    }

    private void drawProjectiles(Graphics2D g){
        g.setColor(CI.getStraightProjectileColor());
        for (StraightProjectile proj : (Iterable<StraightProjectile>)model.getProjectiles()){
            g.fill(proj.getHitBox());
        }
    }

    private void drawEnemies(Graphics2D g){
       
        for (Enemy enemy : model.getEnemies()){
            g.setColor(CI.getEnemyColor(enemy.getHealthPoints()));
            g.fill(enemy.getHitBox());
        }
    }

    private void drawHealthAndScore(Graphics2D g) {
        Rectangle2D board = new Rectangle2D.Double(0,model.getPlayableArea().getHeight(),model.getPlayableArea().getWidth(),20);
        g.setColor(CI.getScoreBoardColor());
        g.draw(board);
        g.setFont(CI.getDefaultFont());
        g.drawString("Health: "+model.getPlayer().getHealthPoints() + "   Score: "+model.getScore(), 3, (int)model.getPlayableArea().getHeight()+18);//(int)model.getPlayableArea().getHeight()
    }
}
