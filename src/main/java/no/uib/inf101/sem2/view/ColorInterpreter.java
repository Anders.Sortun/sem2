package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Font;

/**
 * Class for getting colors, also for interpreting values and giving corresponding color value.
 */
public class ColorInterpreter {

    private static final Color C1 = new Color(255,0,0);
    private static final Color C2 = new Color(180,0,60);
    private static final Color C3 = new Color(125,0,125);
    private static final Color C4 = new Color(60,0,180);
    private static final Color C5 = new Color(0,0,255);

    /**
     * Get color by the amount of healthpoints remaining.
     * @param healthPoints amount of healthpoints left.
     * @return Color.
     */
    Color getEnemyColor(int healthPoints){
        Color color = switch(healthPoints){
            case 1 -> C1;
            case 2 -> C2;
            case 3 -> C3;
            case 4 -> C4;
            case 5 -> C5;
            default -> Color.black;
        };
        return color;
    } 

    Color getPlayerColor(){
        return Color.green;
    }

    Color getStraightProjectileColor(){
        return Color.black;
    }

    Color getGameOverScreenColor(){
        return new Color(255, 0, 0, 50);
    }

    Font getDefaultFont(){
        return new Font("Arial",Font.BOLD,18);
    }

    Color getScoreBoardColor(){
        return Color.BLACK;
    }

}
