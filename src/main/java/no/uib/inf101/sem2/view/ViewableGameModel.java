package no.uib.inf101.sem2.view;

import java.awt.Dimension;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Elements.Enemy;
import no.uib.inf101.sem2.model.Elements.Player;
import no.uib.inf101.sem2.model.Elements.StraightProjectile;

public interface ViewableGameModel {

    /**
     * Get the playerable area of the model, the bounds of all the elements.
     * @return A dimension object representing the bounds.
     */
    Dimension getPlayableArea();

    /**
     * Get the game state of the game.
     * @return a gameState object, GameState.ACTIVE or GameState.GAMEOVER
     */
    GameState getGameState();

    /**
     * Restart the game.
     */
    void restart();

    /**
     * Get the player object.
     * @return the player.
     */
    Player getPlayer();

    /**
     * List of all projectiles currently in play.
     * @return List of StraightProjectiles.
     */
    Iterable<StraightProjectile> getProjectiles();

    /**
     * List of all enemies currently in play.
     * @return List of Enemies.
     */
    Iterable<Enemy> getEnemies();

    /**
     * Get the current score.
     * @return An int representing the score.
     */
    int getScore();
    
}
