package no.uib.inf101.sem2.controller;

/**
 * Keeps track of all the boolean values used to move the player element.
 */
public class PlayerController {
    
    boolean moveUp = false;
    boolean moveDown = false;
    boolean moveLeft = false;
    boolean moveRight = false;

    public boolean getMoveUp(){
        return this.moveUp;
    }

    public boolean getMoveDown(){
        return this.moveDown;
    }

    public boolean getMoveLeft(){
        return this.moveLeft;
    }

    public boolean getMoveRight(){
        return this.moveRight;
    }

    public void setMoveUp(boolean bool){
        this.moveUp = bool;
    }

    public void setMoveDown(boolean bool){
        this.moveDown = bool;
    }

    public void setMoveLeft(boolean bool){
        this.moveLeft = bool;
    }

    public void setMoveRight(boolean bool){
        this.moveRight = bool;
    }
}
