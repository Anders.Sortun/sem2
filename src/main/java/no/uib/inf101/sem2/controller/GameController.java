package no.uib.inf101.sem2.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Timer;

import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.view.GameView;

/**
 * The controller for the game.
 * This object takes keypresses and changes the model accordingly.
 * Also has control on the tickRate for the game, calling clockTick in the model.
 */
public class GameController implements KeyListener{

    private ControllableGameModel model;
    private GameView view;
    private Timer tickRateTimer;
    private Timer straightProjectileTimer;

    /**
     * The constructor, takes a ControllableGameModel and a GameView as parameters.
     * @param model the gameModel.
     * @param view the gameView which represents the gamemodel.
     */
    public GameController(ControllableGameModel model, GameView view){
        this.model = model;
        this.view = view;
        this.tickRateTimer = new Timer(model.getTickRate(), this::clockTick);
        this.straightProjectileTimer = new Timer(100, this::shootStraightProjectile);
        this.straightProjectileTimer.setInitialDelay(0);
        tickRateTimer.start();
        view.addKeyListener(this);
        view.setFocusable(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            model.getPlayerController().setMoveLeft(true);
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            model.getPlayerController().setMoveRight(true);
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            model.getPlayerController().setMoveDown(true);
        }            
        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            model.getPlayerController().setMoveUp(true);
        }
        else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            this.straightProjectileTimer.start();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            model.getPlayerController().setMoveLeft(false);
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            model.getPlayerController().setMoveRight(false);
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            model.getPlayerController().setMoveDown(false);
        }            
        else if (e.getKeyCode() == KeyEvent.VK_UP) {
            model.getPlayerController().setMoveUp(false);
        }
        else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            this.straightProjectileTimer.stop();
        }

        
    }
    
    private void clockTick(ActionEvent event){
        
        if(model.getGameState() == GameState.ACTIVE){
            model.clockTick();
        }
        view.repaint();
    }

    private void shootStraightProjectile(ActionEvent event){
        model.playerShootStraightProjectile();
    }
}
