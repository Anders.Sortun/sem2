package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.GameState;

public interface ControllableGameModel {
 
    

    /**
     * Get the tickRate of the game. 
     * @return milliseconds between each tick.
     */
    int getTickRate();

    /**
     * Get the game state of the game.
     * @return a gameState object, GameState.ACTIVE or GameState.GAMEOVER
     */
    GameState getGameState();

    /**
     * This method is called every time the game "ticks".
     * Also calls the method which moves the player, straightProjectiles and enemies.
     * Every time it checks if some of the shoots hits either enemy or player, and acts accordingly.
     * Also removes dead enemies.
     * Checks if Game Over.
     */
    void clockTick();

    /**
     * Creates an StraightProjectile infront of the player element, with positive velocity.
     */
    void playerShootStraightProjectile();

    /**
     * Get the playerController of the model, which is used to control the player.
     * @return a PlayerController object.
     */
    PlayerController getPlayerController();
}
