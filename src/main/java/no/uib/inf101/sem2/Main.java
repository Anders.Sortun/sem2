package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.GameController;
import no.uib.inf101.sem2.model.EventBus;
import no.uib.inf101.sem2.model.GameModel;
import no.uib.inf101.sem2.view.GameView;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    GameModel model = new GameModel(new Dimension(1280,720),eventBus);
    GameView view = new GameView(model);
    new GameController(model, view);


    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("IDK");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
    frame.setResizable(false);
  }
}
