package no.uib.inf101.sem2.model.Elements;

import java.awt.Dimension;

import no.uib.inf101.sem2.model.Coordinate;

/**
 * StraightProjectile extends the element class, and represents a projectile.
 * This can move in a straight line (along the x axis).
 * Origin is based on which element "shoots" the projectile.
 */
public class StraightProjectile extends Element{
    int velocity;
    String origin;

    /**
     * The constructor for straightProjectile.
     * @param moveableArea the playing field in which all elements are
     * @param hitBoxDimensions how big the projectile is.
     * @param coordinate where on the moveableArea the projectile is.
     * @param velocity how many pixels along the x axis the projectile moves which each tick.
     * @param origin who shoot the projectile, used so that you cant shoot yourself or elements on the same team.
     */
    public StraightProjectile(Dimension moveableArea, Dimension hitBoxDimensions, Coordinate coordinate, int velocity, String origin){
        super(moveableArea,hitBoxDimensions,coordinate);
        this.velocity = velocity;
        this.origin = origin;
    }

    /**
     * Calling this method will shift the coordinate of the projectile by the velocity along the x axis.
     * @return true if movement was possible, else false.
     */
    public boolean nextShot(){
        return this.shiftCoordinateBy(0, velocity);
    }

    public String getOrigin(){
        return this.origin;
    }
}
