package no.uib.inf101.sem2.model.Elements;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.Timer;

import no.uib.inf101.sem2.model.Coordinate;


/**
 * An player is a moveable element that you the player control.
 * It can move around, shot, and die.
 * When shot, the player element is invincible for a few seconds, an ImmunityPhase.
 * When the player's healthpoints reach 0, the game is over.
*/
public class Player extends Element{

    int healthPoints;
    boolean isInvincible;
    Timer invincibleTimer = new Timer(3000, this::turnIsInvincibleToFalse);

    /**
     * Constructs a player element. 
     * When shot, the player isInvincible for a few seconds, an ImmunityPhase.
     * @param healthPoints
     * @param moveableArea
     * @param hitBoxDimensions
     * @param coordinate
     */
    public Player(int healthPoints, Dimension moveableArea, Dimension hitBoxDimensions, Coordinate coordinate){
        super(moveableArea, hitBoxDimensions, coordinate);
        this.healthPoints = healthPoints;
        this.isInvincible = false;
    }

    public int getHealthPoints(){
        return this.healthPoints;
    }

    public void takesDamage(){
        if(this.healthPoints>0){
            this.healthPoints--;
        }
        this.isInvincible = true;
        this.invincibleTimer.start();
    }

    public boolean getIsInvincible(){
        return this.isInvincible;
    }
    
    private void turnIsInvincibleToFalse(ActionEvent l){
        this.isInvincible = false;
        invincibleTimer.stop();
    }

}
