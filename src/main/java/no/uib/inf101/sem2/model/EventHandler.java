package no.uib.inf101.sem2.model;


@FunctionalInterface
interface EventHandler {
    void handle(Event event);
    
}
