package no.uib.inf101.sem2.model.Elements;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

import no.uib.inf101.sem2.model.Coordinate;
import no.uib.inf101.sem2.model.EnemyShootsStraightProjectileEvent;
import no.uib.inf101.sem2.model.EventBus;

/**
 * An enemy extends the element class, and represents a target.
 * It can move around, shoot, die.
 */
public class Enemy extends Element {
    private int healthPoints;
    private Timer shootTimer;
    private EventBus eventBus;

    /**
     * Constructs a new enemy, with int healthpoints, at the coordinate given.
     * It has dimension hitBoxDimension, which together with coordinate will create a rectangle2d at given coordinate and with dimensions
     * 
     * @param eventBus access to same eventbus in model, so that it can shoot.
     * @param healthPoints how many times it can be shot 
     * @param moveableArea dimension of playingarea.
     * @param hitBoxDimensions dimension of hitbox
     * @param coordinate coordinate of the enemy.
     */
    public Enemy(EventBus eventBus,int healthPoints, Dimension moveableArea, Dimension hitBoxDimensions, Coordinate coordinate){
        super(moveableArea, hitBoxDimensions, coordinate);
        this.eventBus = eventBus;
        this.healthPoints = healthPoints;
        this.shootTimer = new Timer(1300, this::shootStraightProjectile);
        shootTimer.start();

    }
 
    /**
     * Calling this method decreases the healthpoints by the int given.
     * If damage is higher than remaining healthpoints, healthpoints is set to 0.
     * @param damage a positive integer.
     */
    public void takesDamage(int damage){
        if(damage>0){
            if(this.healthPoints<=damage){
                this.healthPoints = 0;
            } else{this.healthPoints -= damage;}
        }
    }

    public int getHealthPoints(){
        return this.healthPoints;
    }

    private void shootStraightProjectile(ActionEvent event){
        eventBus.post(new EnemyShootsStraightProjectileEvent(this));
    }

    public void stopShootTimer(){
        this.shootTimer.stop();
    }

    private final int PADDING = 50;
    @Override
    boolean checkIfValidMove(Coordinate coordinate){
       
        if((coordinate.y()<0-PADDING)||(coordinate.y()+getHitBoxDimensions().getHeight()>=moveableArea.getHeight()+PADDING)){
            return false;
        }
        if((coordinate.x()<0-PADDING)||(coordinate.x()+getHitBox().getWidth()>=moveableArea.getWidth()+PADDING)){
            return false;
        }
        return true;
    }
    private int count = 0;
    private int top = 100;
    public void moveOut(){
        if(count<top){
            if(!shiftCoordinateBy(0, -1)){
                this.healthPoints = 0;
            };
            count++;
        }
    }

}
