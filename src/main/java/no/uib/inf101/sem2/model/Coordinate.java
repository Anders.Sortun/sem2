package no.uib.inf101.sem2.model;

/**
 * Coordinate class which represents a point.
 * First y value, then x as parameter
 */
public record Coordinate(int y, int x) {
    
}
