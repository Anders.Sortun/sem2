package no.uib.inf101.sem2.model.Elements;

import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.model.Coordinate;


/**
 * Abstract parent class that represents an element. 
 * It can move around within given bounds.
 * It has an rectangle2d hitbox, created with the coordinate and hitboxdimensions.
 */
public class Element {

    Dimension moveableArea;
    Dimension hitBoxDimensions;
    Coordinate coordinate;
    Rectangle2D hitBox;

    /**
     * A constructor accessible through objects that extends this class.
     * @param moveableArea dimensions of playing area. Used to keep the element within bounds.
     * @param hitBoxDimensions dimensions of the hitbox.
     * @param coordinate coordinate of the element
     */
    public Element(Dimension moveableArea, Dimension hitBoxDimensions, Coordinate coordinate){
        this.moveableArea = moveableArea;
        this.hitBoxDimensions = hitBoxDimensions;
        this.coordinate = coordinate;
        updateHitBox();

    }

    void updateHitBox(){
        this.hitBox = new Rectangle2D.Double(coordinate.x(),coordinate.y(),hitBoxDimensions.getWidth(),hitBoxDimensions.getHeight());
    }

    /**
     * A method which moves the element's coordinate value by two int values.
     * If the move results in the element being out of bounds, or the sides are clipping into the edges, it will return false, and not happen.
     * @param deltaY move coordinate up or down by int pixels.
     * @param deltaX move coordinate left or right by int pixels
     * @return true if move happened, false if move resulted in the hitbox being outside playing field.
     */
    public boolean shiftCoordinateBy(int deltaY, int deltaX){
        Coordinate tempCoordinate = new Coordinate(this.coordinate.y()+deltaY, this.coordinate.x()+deltaX);
        if(checkIfValidMove(tempCoordinate)){
            this.coordinate = tempCoordinate;
            updateHitBox();
            return true;
        } else {
            return false;
        }
        
    }

    boolean checkIfValidMove(Coordinate coordinate){
        if((coordinate.y()<0)||(coordinate.y()+getHitBoxDimensions().getHeight()>=moveableArea.getHeight())){
            return false;
        }
        if((coordinate.x()<0)||(coordinate.x()+getHitBox().getWidth()>=moveableArea.getWidth())){
            return false;
        }
        return true;
    }

    public Dimension getMoveableArea(){
        return this.moveableArea;
    }

    public Dimension getHitBoxDimensions(){
        return this.hitBoxDimensions;
    }

    public Coordinate getCoordinate(){
        return this.coordinate;
    }

    public Rectangle2D getHitBox(){
        return this.hitBox;
    }
}
