package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.model.Elements.Enemy;

public record  EnemyShootsStraightProjectileEvent(Enemy enemy) implements Event{
    
}
