package no.uib.inf101.sem2.model;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.Timer;

import no.uib.inf101.sem2.controller.ControllableGameModel;
import no.uib.inf101.sem2.controller.PlayerController;
import no.uib.inf101.sem2.view.ViewableGameModel;
import no.uib.inf101.sem2.model.Elements.Element;
import no.uib.inf101.sem2.model.Elements.Enemy;
import no.uib.inf101.sem2.model.Elements.Player;
import no.uib.inf101.sem2.model.Elements.StraightProjectile;

/**
 * GameModel represents the game and all the logic behind.
 */
public class GameModel implements ControllableGameModel,ViewableGameModel{

    private EventBus eventBus;
    private Dimension playableArea;
    private Player player;
    private PlayerController playerController = new PlayerController();
    private List<StraightProjectile> listStraightProjectiles = new ArrayList<>();
    private List<Enemy> listEnemies = new ArrayList<>();
    private GameState gameState;

    private Timer newEnemyTimer = new Timer(6000, this::spawnNewEnemyAtRandomY);
    private Random rand = new Random();
    private int score;


    /**
     * The constructor for gameModel.
     * The Dimension dim is given to to all element as a boarder.
     * @param dim a dimension representing the playing area.
     * @param eventBus eventbus used for receiving events.
     */
    public GameModel(Dimension dim, EventBus eventBus){
        this.playableArea = dim;
        this.eventBus = eventBus;
        this.eventBus.register(this::EnemyShootsStraightProjectile);
        restart();
    }

    /**
     * This method will clear the enemy and straightProjectile lists, and spawn in a new player.
     * Thereafter restart the newEnemyTimer, reset score, and set GameState to ACTIVE.
     */
    public void restart(){
        this.player = new Player(3,this.playableArea,new Dimension(30,15), new Coordinate((int)this.playableArea.getHeight()/2, 50));
        this.listStraightProjectiles.clear();;
        this.listEnemies.clear();
        
        this.newEnemyTimer.restart();
        this.gameState = GameState.ACTIVE;
        this.score = 0;
        updateScore(0);
    }


    private int spawnCount = 1;
    private void spawnNewEnemyAtRandomY(ActionEvent l) {
        Dimension hitBoxDimensions = new Dimension(30, 30);
        for(int i = 0; i<spawnCount;i++){
            Coordinate coordinate = new Coordinate(30*rand.nextInt(22), (int)getPlayableArea().getWidth());
            this.listEnemies.add(new Enemy(this.eventBus, 5, getPlayableArea(), hitBoxDimensions, coordinate));
        }
        
    }

    /**
     * This moves the player by the values given.
     * @param deltaY shift the y value by int deltaY.
     * @param deltaX shift the x value by int deltaX.
     * @return True if move happened, else false.
     */
    public boolean movePlayer(int deltaY, int deltaX){
        return this.player.shiftCoordinateBy(deltaY, deltaX);
    }

    public Dimension getPlayableArea(){
        return this.playableArea;
    }

    public Player getPlayer(){
        return this.player;
    }

    public List<StraightProjectile> getProjectiles(){
        return this.listStraightProjectiles;
    }

    public List<Enemy> getEnemies(){
        return this.listEnemies;
    }

    public int getTickRate() {
        return 15;
    }

    public GameState getGameState(){
        return this.gameState;
    }

    public int getScore(){
        return this.score;
    }

    public PlayerController getPlayerController(){
        return this.playerController;
    }

    /**
     * This method calls the shootStraightProjectile on the player with a positive velocity.
     */
    public void playerShootStraightProjectile(){
        shootStraightProjectile(getPlayer(), 7);
    }

    private void EnemyShootsStraightProjectile(Event event) {
        if(event instanceof EnemyShootsStraightProjectileEvent myEvent){
            shootStraightProjectile(myEvent.enemy(),-3);
        }
    }

    private void shootStraightProjectile(Element elem, int velocity){
        if(elem instanceof Player player){
            Coordinate frontmidPlayer = new Coordinate((int)(player.getCoordinate().y() + player.getHitBoxDimensions().getHeight()/2), (int)(player.getCoordinate().x()+player.getHitBoxDimensions().getWidth()));
            listStraightProjectiles.add(new StraightProjectile(playableArea, new Dimension(3,2), frontmidPlayer, velocity,"Player"));
        } else if(elem instanceof Enemy enemy) {
            Coordinate backmidenemy = new Coordinate((int)(enemy.getCoordinate().y() + enemy.getHitBoxDimensions().getHeight()/2), enemy.getCoordinate().x()-3);
            listStraightProjectiles.add(new StraightProjectile(playableArea, new Dimension(5,4), backmidenemy, velocity,"Enemy"));
        }
    }

    private void checkForIntersections(){
        List<StraightProjectile> removeList = new ArrayList<>();

        //When a projectile hits either enemy or player.
        for(StraightProjectile proj : getProjectiles()){
            for(Enemy enemy : getEnemies()){
                if(checkIfElementIntersectsWithElement(proj, enemy)&&!proj.getOrigin().equals("Enemy")){
                    removeList.add(proj);
                    enemy.takesDamage(1);
                }
            }
            if(checkIfElementIntersectsWithElement(proj, player)&&!proj.getOrigin().equals("Player")){
                removeList.add(proj);
                if(!player.getIsInvincible()){
                    player.takesDamage();
                }
            }
        }

        //When enemies and player intersects.
        for (Enemy enemy : getEnemies()){
            if(checkIfElementIntersectsWithElement(getPlayer(), enemy)){
                enemy.takesDamage(5);
                player.takesDamage();
            }
        }

        //Removes all elements in removeList
        for (StraightProjectile proj : removeList){
            removeProjectile(proj);
        }
    }

    private void removeProjectile(StraightProjectile projectile){
        getProjectiles().remove(projectile);
    }

    private boolean checkIfElementIntersectsWithElement(Element elem1, Element elem2){

        Coordinate coorTRelem1 = new Coordinate(elem1.getCoordinate().y(), (int)(elem1.getCoordinate().x() + elem1.getHitBoxDimensions().getWidth()));
        Coordinate coorTRelem2 = new Coordinate(elem2.getCoordinate().y(), (int)(elem2.getCoordinate().x() + elem2.getHitBoxDimensions().getWidth()));
        Coordinate coorBLelem2 = new Coordinate(elem2.getCoordinate().y() + (int)(elem2.getHitBoxDimensions().getHeight()), (int)(elem2.getCoordinate().x()));
        
        if(((coorTRelem1.x()<=coorTRelem2.x())&&(coorTRelem1.y()>=coorTRelem2.y()))&&((coorTRelem1.x()>=coorBLelem2.x())&&(coorTRelem1.y()<=coorBLelem2.y()))){
            return true;
        }
        return false;
    }

    private void updateScore(int x){
        this.score += 50 * x;
        this.spawnCount = score/500 +1;
    }
    
    

    
    /**
     * This method is called every time the game "ticks".
     * Also calls the method which moves the player, straightProjectiles and enemies.
     * Every time it checks if some of the shoots hits either enemy or player, and acts accordingly.
     * Also removes dead enemies.
     * Checks if Game Over.
     */
    public void clockTick(){
        if(playerController.getMoveUp()){
            movePlayer(-4, 0);
        }
        if(playerController.getMoveDown()){
            movePlayer(4, 0);
        }
        if(playerController.getMoveRight()){
            movePlayer(0, 4);
        }
        if(playerController.getMoveLeft()){
            movePlayer(0, -4);
        }
        
        moveOrRemoveStraightProjectiles();
        moveEnemies();
        checkForIntersections();
        removeDeadEnemies();
        checkIfGameOver();
    }

    private void removeDeadEnemies(){
        List<Enemy> removeList = new ArrayList<>();
        for(Enemy enemy : getEnemies()){
            if(enemy.getHealthPoints()<=0){
                removeList.add(enemy);
            }
        }
        if(removeList.size()>0){
            updateScore(removeList.size());
        }
        for(Enemy enemy : removeList){
            enemy.stopShootTimer();
            getEnemies().remove(enemy);
        }
    }

    private void checkIfGameOver(){
        if(getPlayer().getHealthPoints()<=0){
            this.gameState = GameState.GAME_OVER;
            this.newEnemyTimer.stop();
            for(Enemy enemy : getEnemies()){
                enemy.stopShootTimer();
            }
        }
    }

    private void moveOrRemoveStraightProjectiles(){
        List<StraightProjectile> removeList = new ArrayList<>();
        for (StraightProjectile proj : getProjectiles()){
            if(!proj.nextShot()){
                removeList.add(proj);
            };
        }
        for (StraightProjectile proj : removeList){
            removeProjectile(proj);
        }
    }

    private void moveEnemies(){
        for (Enemy enemy : getEnemies()){
            enemy.moveOut();
        }
    }


}
