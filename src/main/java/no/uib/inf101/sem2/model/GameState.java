package no.uib.inf101.sem2.model;

public enum GameState {
    ACTIVE,
    GAME_OVER;
}
